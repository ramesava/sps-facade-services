package com.scholastic.facade.Automation;




import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.ResponseSpecification;

	public class SPSGradeClassSizeServiceTest extends BaseTest
	{
		// Teacher Info
		private String teacherfirstName="Robert";
		private String teacherlastName="Sheehan";
		private String email;
		private String password="passed1";
		private String schoolId="163392";
		private List<String> userType1=new ArrayList<String>();
		private String teacherSPSID;
		//private boolean firstYearTeaching = true;
		
		//GradeClassSize Info
		private String grade="02";
		//private List<String> classSize=new ArrayList<String>();
		private String classSize="25";
		private String gradeclasssizeSPSID;
		
		//Update GradeClassSize Info
		private String grade1="04";
		private String classSize1="21";
		
		
		
		
				
		//End Points
		private static final String ENDPOINT_TEACHER_REGISTRATION="/spsuser/?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_TEACHER_GET="/spsuser/{spsId}?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_CREATE_GRADECLASSSIZE="/spsgradeclasssize/?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_GET_UPDATE_DELETE_GRADECLASSSIZE="/spsgradeclasssize/{spsId}?clientId=SPSFacadeAPI";
		
		
		
			

		
	@Test
		public void createGradeClassSizeGetGradeClassSizeTest()
		{
			System.out.println("###############################################################");	
			System.out.println("*******  Crate GradeClassSize and Get the GradeClassSize*******");
			System.out.println("###############################################################");
			
			// Light Teacher Registration
			ExtractableResponse<Response> createTeacherResponse=
					given()
							.log().all()
							.contentType("application/json")
							.body(createTeacherPayload()).
					when()
							.post(ENDPOINT_TEACHER_REGISTRATION).
					then()
							.statusCode(201)
							.spec(createTeacherResponseValidator())
							.extract();		
			
			teacherSPSID=createTeacherResponse.path("spsId");	
			System.out.println(teacherSPSID);
						
			// Create GradeClassSize
			ExtractableResponse<Response> createGradeClassSizeResponse=
					given()
							.log().all()
							.contentType("application/json")
							.body(createGradeClassSizePayload()).
					when()
							.post(ENDPOINT_CREATE_GRADECLASSSIZE).
					then()
							.statusCode(200)
							.spec(createGradeClassSizeResponseValidator())
							.extract();
			gradeclasssizeSPSID=createGradeClassSizeResponse.path("spsId");	
			System.out.println("gradeclasssizeSPSID is: "+gradeclasssizeSPSID);
			
			//System.out.println(createGradeClassSizeResponse.asString());
			
			// Get GradeClassSize 
			ExtractableResponse<Response> getGradeClassSizeResponse=
					given()
							.pathParam("spsId",gradeclasssizeSPSID).
					when()
							.get(ENDPOINT_GET_UPDATE_DELETE_GRADECLASSSIZE).
					then()
							.statusCode(200)
							.extract();	
			System.out.println("********** Grade Class Size Info **********");
			System.out.println(getGradeClassSizeResponse.asString());
			System.out.println("********************************************");
			
			
	}	
	
	@Test
	public void updateGradeClassSizeGetGradeClassSizeTest()
	{
		System.out.println("######################################################################");	
		System.out.println("******* Create GradeClassSize, Update and Get the GradeClassSize*******");
		System.out.println("#######################################################################");
		
		// Light Teacher Registration
		ExtractableResponse<Response> createTeacherResponse=
				given()
						.log().all()
						.contentType("application/json")
						.body(createTeacherPayload()).
				when()
						.post(ENDPOINT_TEACHER_REGISTRATION).
				then()
						.statusCode(201)
						.spec(createTeacherResponseValidator())
						.extract();		
		
		teacherSPSID=createTeacherResponse.path("spsId");	
		System.out.println(teacherSPSID);
					
		// Create GradeClassSize
		ExtractableResponse<Response> createGradeClassSizeResponse=
				given()
						.log().all()
						.contentType("application/json")
						.body(createGradeClassSizePayload()).
				when()
						.post(ENDPOINT_CREATE_GRADECLASSSIZE).
				then()
						.statusCode(200)
						.spec(createGradeClassSizeResponseValidator())
						.extract();
		gradeclasssizeSPSID=createGradeClassSizeResponse.path("spsId");	
		System.out.println("gradeclasssizeSPSID is: "+gradeclasssizeSPSID);
				
		// Update GradeClassSize
		ExtractableResponse<Response> updateGradeClassSizeResponse=
				given()
						.log().all()
						.pathParam("spsId",gradeclasssizeSPSID)
						.contentType("application/json")
						.body(updateGradeClassSizePayload()).
				when()
						.put(ENDPOINT_GET_UPDATE_DELETE_GRADECLASSSIZE).
				then()
						.statusCode(200)
						.spec(updateGradeClassSizeResponseValidator())
						.extract();
		gradeclasssizeSPSID=updateGradeClassSizeResponse.path("spsId");	
		System.out.println("gradeclasssizeSPSID is: "+gradeclasssizeSPSID);	
		
		// Get GradeClassSize 
		ExtractableResponse<Response> getUpdatedGradeClassSizeResponse=
				given()
						.pathParam("spsId",gradeclasssizeSPSID).
				when()
						.get(ENDPOINT_GET_UPDATE_DELETE_GRADECLASSSIZE).
				then()
						.statusCode(200)
						.extract();	
		System.out.println("********** Updated Grade Class Size Info **********");
		System.out.println(getUpdatedGradeClassSizeResponse.asString());
		System.out.println("***************************************************");
		
		System.out.println("********** Grade Class Size Info **********");
		System.out.println(createGradeClassSizeResponse.asString());
		System.out.println("***************************************************");
			
}
	
	@Test
	public void deleteGradeClassSizeGetGradeClassSizeTest()
	{
		System.out.println("######################################################################");	
		System.out.println("******* Create GradeClassSize, Delete and Get the GradeClassSize*******");
		System.out.println("#######################################################################");
		
		// Light Teacher Registration
		ExtractableResponse<Response> createTeacherResponse=
				given()
						.log().all()
						.contentType("application/json")
						.body(createTeacherPayload()).
				when()
						.post(ENDPOINT_TEACHER_REGISTRATION).
				then()
						.statusCode(201)
						.spec(createTeacherResponseValidator())
						.extract();		
		
		teacherSPSID=createTeacherResponse.path("spsId");	
		System.out.println(teacherSPSID);
					
		// Create GradeClassSize
		ExtractableResponse<Response> createGradeClassSizeResponse=
				given()
						.log().all()
						.contentType("application/json")
						.body(createGradeClassSizePayload()).
				when()
						.post(ENDPOINT_CREATE_GRADECLASSSIZE).
				then()
						.statusCode(200)
						.spec(createGradeClassSizeResponseValidator())
						.extract();
		gradeclasssizeSPSID=createGradeClassSizeResponse.path("spsId");	
		System.out.println("gradeclasssizeSPSID is: "+gradeclasssizeSPSID);
				
		// Delete GradeClassSize
		ExtractableResponse<Response> deleteGradeClassSizeResponse=
				given()
						.log().all()
						.pathParam("spsId",gradeclasssizeSPSID)
						.contentType("application/json").
				when()
						.delete(ENDPOINT_GET_UPDATE_DELETE_GRADECLASSSIZE).
				then()
						.statusCode(200)
						//.spec(updateGradeClassSizeResponseValidator())
						.extract();
				
		System.out.println("********** Deleted Grade Class Size ***************");
		System.out.println(deleteGradeClassSizeResponse.asString());
		System.out.println("***************************************************");
		
	}
		
		private String getEmail()
		{
			email="sps_qa"+String.valueOf(System.currentTimeMillis())+"@sample.com";
			return email;
		
		}
		
		private Map<String, Object> createTeacherPayload()
		
		{
			Map<String, Object> teacherRegistrationInfo = new HashMap<String, Object>();
			teacherRegistrationInfo.put("firstName",teacherfirstName);
			teacherRegistrationInfo.put("lastName",teacherlastName);
			teacherRegistrationInfo.put("email",getEmail());
			userType1.add("TEACHER");
			teacherRegistrationInfo.put("userType", userType1);
			teacherRegistrationInfo.put("password",password);
			teacherRegistrationInfo.put("userName",email);
			teacherRegistrationInfo.put("schoolId",schoolId);
			return teacherRegistrationInfo;
			
		}

		private Map<String, Object> createGradeClassSizePayload()
				
		{
			Map<String, Object> gradeClassSizeInfo = new HashMap<String, Object>();
			gradeClassSizeInfo.put("grade",grade);
			gradeClassSizeInfo.put("classSize",classSize);
			gradeClassSizeInfo.put("spsUserId",teacherSPSID);
			return gradeClassSizeInfo;
			
		}
		
		private Map<String, Object> updateGradeClassSizePayload()
		
		{
			Map<String, Object> gradeClassSizeUpdateInfo = new HashMap<String, Object>();
			gradeClassSizeUpdateInfo.put("grade",grade1);
			gradeClassSizeUpdateInfo.put("classSize",classSize1);
			gradeClassSizeUpdateInfo.put("spsUserId",teacherSPSID);
			return gradeClassSizeUpdateInfo;
			
		}
		
		
				
		private ResponseSpecification createTeacherResponseValidator()
		
		//Light Teacher Registration Validation
		{
				ResponseSpecBuilder rspec=new ResponseSpecBuilder()
				.expectBody("userName",equalTo(email))
				//.expectBody("modifiedDate",equalTo(""))
				//.expectBody("registrationDate",equalTo(new Date()))
				.expectBody("schoolId",equalTo(schoolId))
				.expectBody("orgZip",is(not(empty())))
				.expectBody("userType",equalTo(userType1))
				.expectBody("isEducator",is(not(empty())))
				.expectBody("cac",is(not(empty())))
				.expectBody("cacId",is(not(empty())))
				.expectBody("isIdUsed",is(not(empty())))
				.expectBody("isEnabled",is(not(empty())))
				.expectBody("schoolUcn",is(not(empty())))
				.expectBody("email",equalTo(email))
				.expectBody("firstName",equalTo(teacherfirstName))
				.expectBody("lastName",equalTo(teacherlastName))
				//.expectBody("password",equalTo(password))
				.expectBody("spsId",is(not(empty())));
				return rspec.build();		
		}
		
		private ResponseSpecification createGradeClassSizeResponseValidator()
		
		//Light Parent Registration Validation
		{
				ResponseSpecBuilder rspec=new ResponseSpecBuilder()
				.expectBody("grade",equalTo(grade))
				.expectBody("classSize",equalTo(classSize))
				.expectBody("spsUserId",equalTo(teacherSPSID))
				.expectBody("spsId",is(not(empty())));
				return rspec.build();		
		}
		
private ResponseSpecification updateGradeClassSizeResponseValidator()
		
		//Light Parent Registration Validation
		{
				ResponseSpecBuilder rspec=new ResponseSpecBuilder()
				.expectBody("grade",equalTo(grade1))
				.expectBody("classSize",equalTo(classSize1))
				.expectBody("spsUserId",equalTo(teacherSPSID))
				.expectBody("spsId",is(not(empty())));
				return rspec.build();		
		}		
		


	}
