package com.scholastic.facade.Automation;




import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.ResponseSpecification;

	public class SPSWalletServiceTest extends BaseTest
	{
		// Login for Session Id and TSP Id
		private String user;
		private String password ="passed1";
		private boolean singleToken = true;
		private boolean userForWS = false;
		private boolean addCookie = true;
		private boolean addExpiration = true;
		private String sps_session;
		private String sps_tsp;
		private String userSPSID;
		private List<Integer> walletID=new ArrayList<Integer>();
		private Integer totalcreditcardCount;
		
		// Teacher Info
		
		//private String title="MR";
		private String firstName="Kerry";
		private String lastName="Moore";
		private String email;
		private String schoolId="453926";
		private List<String> userType=new ArrayList<String>();
		private String teacherSPSID;
		
		// Add Credit Card
		 private String   reqBillToAddressCountry="US";  
		 private String   authAvsCode="Y";  
		 private String   reqBillToPhone="7189916545";  
		 private String   reqMerchantDefinedData35="anythingelse";  
		 private String   reqCardNumber="411111xxxxxx1111";  
		 private String   reqCardExpiryDate="03-2017";  
		 private String   decision="ACCEPT";  
		 private String   reqBillToAddressState="NY";  
		 private String   signedFieldNames="transaction_id_XXX SET ALL";  
		 //private String   reqMerchantDefinedData12;  
		 private String   reqPaymentMethod="card";  
		 private String   reqTransactionType="create_payment_token";  
		 private String   authCode="831000";  
		 private String   signature="XSDSSDSDFSDFSDFSDFDFDFdffDFDSDF";  
		 private String   reqLocale="en";  
		 private String   reasonCode="100";  
		 private String   reqBillToAddressPostalCode="11372";  
		 private String   reqBillToAddressLine2="line2";  
		 private String   reqBillToAddressLine1="34-21 70th Street";  
		 private String   reqCardType="001";  
		 private String   authAmount="000";  
		 private String   reqBillToAddressCity="Jackson Heights";  
		 private String   signedDateTime="2015-06-09T21:17:33Z";  
		 private String   reqCurrency="USD";  
		 private String   reqMerchantDefinedData25="ADD";  
		 private String   reqReferenceNumber="SCH_SDSDSDDS";  
		 private String   reqMerchantDefinedData22="-1";  
		 private String   reqMerchantDefinedData23="true";  
		 private String   reqMerchantDefinedData21="true";  
		 private String   authAvsCodeRaw="Y";  
		 private String   transactionId="4333XXXXXXXXXX";  
		 private String   authTime="2015-06XXXXX";  
		 private String   message="request+was+processed+succesfully";  
		 private String   authResponse="85";  
		// private String   reqConsumerId;  
		 private String   reqProfileId="MAAWSQA1";  
		 private String   reqTransactionUuId="b5sdfsdfdsfdsf";  
		 private String   reqPaymentToken="01019648212121";  
		 private String   authTransRefNo="WWNTYDFDFD";  
		 //private String   reqBillToSurname;  
		 //private String   reqBillToForename;  
		// private String   reqBillToEmail;  
		 private String   accessKey="84854545454564564564private String";
		 
		 //Update Credit Card
		 private String   u_reqCardExpiryDate="04-2018";
		 private String   u_reqBillToAddressLine1="89-24 215th Street";
		 private String   u_reqBillToAddressCity="Queens Village";
		 private String   u_reqBillToAddressPostalCode="11427";
			
				
		//End Points
		private static final String ENDPOINT_TEACHER_REGISTRATION="/spsuser/?clientId=SPSFacadeAPI"; 
		private static final String ENDPOINT_ADD_CREDITCARD="/spsuser/{spsId}/cybersource/creditcard?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_GET_CREDITCARD="/spsuser/{spsId}/cybersource/creditcard?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_UPDATE_CREDITCARD="/spsuser/{spsId}/cybersource/creditcard/0?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_DELETE_CREDITCARD="/spsuser/{spsId}/cybersource/creditcard/0?clientId=SPSFacadeAPI";
		
				
		@Test
		public void addCreditCardToWalletTest() throws InterruptedException
		{	
			System.out.println("###################################################################");
			System.out.println("******************** Add Credit Card To Wallet ********************");
			System.out.println("###################################################################");
			
			//Create Teacher
			ExtractableResponse<Response> createTeacherResponse=
					given()
							.log().all()
							.contentType("application/json")
							.body(createTeacherPayload()).
					when()
							.post(ENDPOINT_TEACHER_REGISTRATION).
					then()
							.statusCode(201)
							.spec(createTeacherResponseValidator())
							.extract();		
			teacherSPSID=createTeacherResponse.path("spsId");
			user=createTeacherResponse.path("userName");
			System.out.println(">>>>>>>Teacher SPSID is: "+teacherSPSID);
			System.out.println(">>>>>>>User Name is: "+user);
			
			Thread.sleep(9000L);
			
			//Authentication
			ExtractableResponse<Response> createLogInResponse=
					given()
							.contentType("application/json")
			                .body(createLogInPayload()).
					when()
							.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
					then()
							.statusCode(200)
							.extract();	
			sps_session=createLogInResponse.path("SPS_SESSION.value");
			sps_tsp=createLogInResponse.path("SPS_TSP.value");
			System.out.println(createLogInResponse.path("SPS_UD.value"));
			userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
			
			System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
			System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
			System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
			
						
			// Add Credit card to wallet.
			
			ExtractableResponse<Response> addCreditCardToWalletResponse=
					given()
							.log().all()
							.pathParam("spsId",userSPSID)
							.cookies("SPS_SESSION",sps_session)
							.cookie("SPS_TSP",sps_tsp)
							.contentType("application/json")
							.body(addCreditCardToWalletPayload()).
					when()
							.post(ENDPOINT_ADD_CREDITCARD).
					then()
							.statusCode(200)
							//.spec(addCreditCardToWalletResponseValidator())
							.extract();
			System.out.println("******************** AddCredit Card To Wallet Response *********************");
			System.out.println(addCreditCardToWalletResponse.asString());
			System.out.println("****************************************************************************");
				
		}
		
		@Test
		public void addCreditCardToWalletAndGetTest()throws InterruptedException 
		{	
			System.out.println("###########################################################################");
			System.out.println("******************** Add Credit Card To Wallet and Get ********************");
			System.out.println("###########################################################################");
			
			//Create Teacher
			ExtractableResponse<Response> createTeacherResponse=
					given()
							.log().all()
							.contentType("application/json")
							.body(createTeacherPayload()).
					when()
							.post(ENDPOINT_TEACHER_REGISTRATION).
					then()
							.statusCode(201)
							.spec(createTeacherResponseValidator())
							.extract();		
			teacherSPSID=createTeacherResponse.path("spsId");
			user=createTeacherResponse.path("userName");
			System.out.println(">>>>>>>Teacher SPSID is: "+teacherSPSID);
			System.out.println(">>>>>>>User Name is: "+user);
			
			Thread.sleep(8000L);
			//Authentication
			ExtractableResponse<Response> createLogInResponse=
					given()
							.contentType("application/json")
			                .body(createLogInPayload()).
					when()
							.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
					then()
							.statusCode(200)
							.extract();	
			sps_session=createLogInResponse.path("SPS_SESSION.value");
			sps_tsp=createLogInResponse.path("SPS_TSP.value");
			System.out.println(createLogInResponse.path("SPS_UD.value"));
			userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
			
			System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
			System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
			System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
			
			// Add Credit card to wallet.
			
			ExtractableResponse<Response> addCreditCardToWalletResponse=
					given()
							.log().all()
							.pathParam("spsId",userSPSID)
							.cookies("SPS_SESSION",sps_session)
							.cookie("SPS_TSP",sps_tsp)
							.contentType("application/json")
							.body(addCreditCardToWalletPayload()).
					when()
							.post(ENDPOINT_ADD_CREDITCARD).
					then()
							.statusCode(200)
							//.spec(addCreditCardToWalletResponseValidator())
							.extract();
			
			System.out.println(addCreditCardToWalletResponse.asString());
						
			// Get the Credit Card
			
			ExtractableResponse<Response> getCreditCardResponse=
					given()
							.pathParam("spsId",teacherSPSID).
					when()
							.get(ENDPOINT_GET_CREDITCARD).
					then()
							.statusCode(200)
							.spec(getCreditCardResponseValidator())
							.extract();
			totalcreditcardCount= getCreditCardResponse.path("count");
			int walletId=Integer.parseInt((String) getCreditCardResponse.path("wallet.id[0]"));
			walletID.add(walletId);
			System.out.println(">>>>>>>>> Wallet Id is: "+walletId);
			//System.out.println("@#######"+walletID.get(0));
			System.out.println(">>>>>>>>> Total Credit Card Count is: "+totalcreditcardCount);
			System.out.println("******************** Get Credit Card Info From Wallet *********************");
			System.out.println(getCreditCardResponse.asString());
			System.out.println("****************************************************************************");
				
					
		}
		
		@Test
		public void updateCreditCardAndGetTest() throws InterruptedException
		{	
			System.out.println("###########################################################################");
			System.out.println("******************** Update Credit Card Get *******************************");
			System.out.println("###########################################################################");
			
			//Create Teacher
			ExtractableResponse<Response> createTeacherResponse=
					given()
							.log().all()
							.contentType("application/json")
							.body(createTeacherPayload()).
					when()
							.post(ENDPOINT_TEACHER_REGISTRATION).
					then()
							.statusCode(201)
							.spec(createTeacherResponseValidator())
							.extract();		
			teacherSPSID=createTeacherResponse.path("spsId");
			user=createTeacherResponse.path("userName");
			System.out.println(">>>>>>>Teacher SPSID is: "+teacherSPSID);
			System.out.println(">>>>>>>User Name is: "+user);
			
			Thread.sleep(4000L);
			//Authentication
			ExtractableResponse<Response> createLogInResponse=
					given()
							.contentType("application/json")
			                .body(createLogInPayload()).
					when()
							.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
					then()
							.statusCode(200)
							.extract();	
			sps_session=createLogInResponse.path("SPS_SESSION.value");
			sps_tsp=createLogInResponse.path("SPS_TSP.value");
			System.out.println(createLogInResponse.path("SPS_UD.value"));
			userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
			
			System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
			System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
			System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
			
			
			// Add Credit card to wallet.
			
			ExtractableResponse<Response> addCreditCardToWalletResponse=
					given()
							.log().all()
							.pathParam("spsId",userSPSID)
							.cookies("SPS_SESSION",sps_session)
							.cookie("SPS_TSP",sps_tsp)
							.contentType("application/json")
							.body(addCreditCardToWalletPayload()).
					when()
							.post(ENDPOINT_ADD_CREDITCARD).
					then()
							.statusCode(200)
							//.spec(addCreditCardToWalletResponseValidator())
							.extract();
			System.out.println(addCreditCardToWalletResponse.asString());
									
			// Update the Credit Card
			
			ExtractableResponse<Response> updateCreditCardResponse=
					given()
							.pathParam("spsId",teacherSPSID)
							//.pathParam("walletid",walletId)
							.cookies("SPS_SESSION",sps_session)
							.cookie("SPS_TSP",sps_tsp)
							.contentType("application/json")
							.body(updateCreditCardPayload()).
					when()
							.put(ENDPOINT_UPDATE_CREDITCARD).
					then()
							.statusCode(200)
							//.spec(updateCreditCardResponseValidator())
							.extract();
			//System.out.println("******************** Update Credit Card Response *********************");
			System.out.println(updateCreditCardResponse.asString());
			//System.out.println("**********************************************************************");
			
			// Get the Credit Card
			
			ExtractableResponse<Response> getUpdatedCreditCardResponse=
					given()
							.pathParam("spsId",teacherSPSID).
					when()
							.get(ENDPOINT_GET_CREDITCARD).
					then()
							.statusCode(200)
							.spec(getUpdatedCreditCardResponseValidator())
							.extract();
			totalcreditcardCount= getUpdatedCreditCardResponse.path("count");
			int walletId=Integer.parseInt((String) getUpdatedCreditCardResponse.path("wallet.id[0]"));
			walletID.add(walletId);
			System.out.println(">>>>>>>>> Wallet Id is: "+walletId);
			//System.out.println("@#######"+walletID.get(0));
			System.out.println(">>>>>>>>> Total Credit Card Count is: "+totalcreditcardCount);
				//System.out.println("@#######"+walletID.get(0));
				System.out.println("******************** Get Updated Credit Card Info From Wallet *********************");
				System.out.println(getUpdatedCreditCardResponse.asString());
				System.out.println("****************************************************************************");
								
					
		}
		
		@Test
		public void deleteCreditCardFromWalletTest() throws InterruptedException
		{	
			System.out.println("#######################################################################");
			System.out.println("******************** Delete Credit Card From Wallet *******************");
			System.out.println("########################################################################");
			
			//Create Teacher
			ExtractableResponse<Response> createTeacherResponse=
					given()
							.log().all()
							.contentType("application/json")
							.body(createTeacherPayload()).
					when()
							.post(ENDPOINT_TEACHER_REGISTRATION).
					then()
							.statusCode(201)
							.spec(createTeacherResponseValidator())
							.extract();		
			teacherSPSID=createTeacherResponse.path("spsId");
			user=createTeacherResponse.path("userName");
			System.out.println(">>>>>>>Teacher SPSID is: "+teacherSPSID);
			System.out.println(">>>>>>>User Name is: "+user);
			
			Thread.sleep(8000L);
			
			//Authentication
			ExtractableResponse<Response> createLogInResponse=
					given()
							.log().all()
							.contentType("application/json")
			                .body(createLogInPayload()).
					when()
							.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
					then()
							.statusCode(200)
							.extract();	
			sps_session=createLogInResponse.path("SPS_SESSION.value");
			sps_tsp=createLogInResponse.path("SPS_TSP.value");
			System.out.println(createLogInResponse.path("SPS_UD.value"));
			userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
			
			System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
			System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
			System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
			
			// Add Credit card to wallet.
			
			ExtractableResponse<Response> addCreditCardToWalletResponse=
					given()
							.log().all()
							.pathParam("spsId",userSPSID)
							.cookies("SPS_SESSION",sps_session)
							.cookie("SPS_TSP",sps_tsp)
							.contentType("application/json")
							.body(addCreditCardToWalletPayload()).
					when()
							.post(ENDPOINT_ADD_CREDITCARD).
					then()
							.statusCode(200)
							//.spec(addCreditCardToWalletResponseValidator())
							.extract();
			
			System.out.println("******************** Response After Adding Credit Card To Wallet *********************");
			System.out.println(addCreditCardToWalletResponse.asString());
			System.out.println("**************************************************************************************");			
			// Delete Credit Card From Wallet
			
			ExtractableResponse<Response> deleteCreditCardResponse=
					given()
							.pathParam("spsId",teacherSPSID).
					when()
							.delete(ENDPOINT_DELETE_CREDITCARD).
					then()
							.statusCode(200)
							//.spec(deleteCreditCardResponseValidator())
							.extract();
			System.out.println("******************** Response After Deleting Credit Card From Wallet *********************");
			System.out.println(deleteCreditCardResponse.asString());
			System.out.println("*******************************************************************************************");
					
		}
		
		
		private String getEmail()
		{
			 String email="sps_qa"+String.valueOf(System.currentTimeMillis())+"@sample.com";
			 return email;
		
		}
		
			private Map<String, Object> createLogInPayload()

		{
			Map<String, Object> createLogInInfo = new HashMap<String, Object>();
			createLogInInfo.put("user",user );
			createLogInInfo.put("password",password);
			createLogInInfo.put("singleToken",singleToken);
			createLogInInfo.put("userForWS", userForWS);
			createLogInInfo.put("addCookie",addCookie);
			createLogInInfo.put("addExpiration",addExpiration);
			return createLogInInfo;
		
		}
		
			private Map<String, Object> createTeacherPayload()
		
		{
			if(email==null)
			{
				email=getEmail();
			}
			Map<String, Object> teacherRegistrationInfo = new HashMap<String, Object>();
			teacherRegistrationInfo.put("firstName",firstName);
			teacherRegistrationInfo.put("lastName",lastName);
			teacherRegistrationInfo.put("email",email);
			userType.add("TEACHER");
			teacherRegistrationInfo.put("userType", userType);
			teacherRegistrationInfo.put("password",password);
			teacherRegistrationInfo.put("userName",email);
			teacherRegistrationInfo.put("schoolId",schoolId);
			return teacherRegistrationInfo;
			
		}
			
			private Map<String, Object> addCreditCardToWalletPayload()
		{					
			 Map<String, Object> addCreditCardInfo = new HashMap<String, Object>();
			 addCreditCardInfo.put("reqBillToAddressCountry",reqBillToAddressCountry);  
			 addCreditCardInfo.put("authAvsCode",authAvsCode);  
			 addCreditCardInfo.put("reqBillToPhone",reqBillToPhone);  
			 addCreditCardInfo.put("reqMerchantDefinedData35",reqMerchantDefinedData35);  
			 addCreditCardInfo.put("reqCardNumber",reqCardNumber);  
			 addCreditCardInfo.put("reqCardExpiryDate",reqCardExpiryDate);  
			 addCreditCardInfo.put("decision",decision);  
			 addCreditCardInfo.put("reqBillToAddressState",reqBillToAddressState);  
			 addCreditCardInfo.put("signedFieldNames",signedFieldNames);  
			 addCreditCardInfo.put("reqMerchantDefinedData12",userSPSID);  
			 addCreditCardInfo.put("reqPaymentMethod",reqPaymentMethod);  
			 addCreditCardInfo.put("reqTransactionType",reqTransactionType);  
			 addCreditCardInfo.put("authCode",authCode);  
			 addCreditCardInfo.put("signature",signature);  
			 addCreditCardInfo.put("reqLocale",reqLocale);  
			 addCreditCardInfo.put("reasonCode",reasonCode);  
			 addCreditCardInfo.put("reqBillToAddressPostalCode",reqBillToAddressPostalCode);  
			 addCreditCardInfo.put("reqBillToAddressLine2",reqBillToAddressLine2);  
			 addCreditCardInfo.put("reqBillToAddressLine1",reqBillToAddressLine1);  
			 addCreditCardInfo.put("reqCardType",reqCardType);  
			 addCreditCardInfo.put("authAmount",authAmount);  
			 addCreditCardInfo.put("reqBillToAddressCity",reqBillToAddressCity);  
			 addCreditCardInfo.put("signedDateTime",signedDateTime);  
			 addCreditCardInfo.put("reqCurrency",reqCurrency);  
			 addCreditCardInfo.put("reqMerchantDefinedData25",reqMerchantDefinedData25);  
			 addCreditCardInfo.put("reqReferenceNumber",reqReferenceNumber);  
			 addCreditCardInfo.put("reqMerchantDefinedData22",reqMerchantDefinedData22);  
			 addCreditCardInfo.put("reqMerchantDefinedData23",reqMerchantDefinedData23);  
			 addCreditCardInfo.put("reqMerchantDefinedData21",reqMerchantDefinedData21);  
			 addCreditCardInfo.put("authAvsCodeRaw",authAvsCodeRaw);  
			 addCreditCardInfo.put("transactionId",transactionId);  
			 addCreditCardInfo.put("authTime",authTime);  
			 addCreditCardInfo.put("message",message);  
			 addCreditCardInfo.put("authResponse",authResponse);  
			 addCreditCardInfo.put("reqConsumerId",userSPSID);  
			 addCreditCardInfo.put("reqProfileId",reqProfileId);  
			 addCreditCardInfo.put("reqTransactionUuId",reqTransactionUuId);  
			 addCreditCardInfo.put("reqPaymentToken",reqPaymentToken);  
			 addCreditCardInfo.put("authTransRefNo",authTransRefNo);  
			 addCreditCardInfo.put("reqBillToSurname",lastName);  
			 addCreditCardInfo.put("reqBillToForename",firstName);  
			 addCreditCardInfo.put("reqBillToEmail",email);  
			 addCreditCardInfo.put("accessKey",accessKey);
			 return addCreditCardInfo;
				
		}
			
			private Map<String, Object> updateCreditCardPayload()
		{					
			 Map<String, Object> updateCreditCardInfo = new HashMap<String, Object>();
			 updateCreditCardInfo.put("reqBillToAddressCountry",reqBillToAddressCountry);  
			 updateCreditCardInfo.put("authAvsCode",authAvsCode);  
			 updateCreditCardInfo.put("reqBillToPhone",reqBillToPhone);  
			 updateCreditCardInfo.put("reqMerchantDefinedData35",reqMerchantDefinedData35);  
			 updateCreditCardInfo.put("reqCardNumber",reqCardNumber);  
			 updateCreditCardInfo.put("reqCardExpiryDate",u_reqCardExpiryDate);  
			 updateCreditCardInfo.put("decision",decision);  
			 updateCreditCardInfo.put("reqBillToAddressState",reqBillToAddressState);  
			 updateCreditCardInfo.put("signedFieldNames",signedFieldNames);  
			 updateCreditCardInfo.put("reqMerchantDefinedData12",userSPSID);  
			 updateCreditCardInfo.put("reqPaymentMethod",reqPaymentMethod);  
			 updateCreditCardInfo.put("reqTransactionType",reqTransactionType);  
			 updateCreditCardInfo.put("authCode",authCode);  
			 updateCreditCardInfo.put("signature",signature);  
			 updateCreditCardInfo.put("reqLocale",reqLocale);  
			 updateCreditCardInfo.put("reasonCode",reasonCode);  
			 updateCreditCardInfo.put("reqBillToAddressPostalCode",u_reqBillToAddressPostalCode);  
			 updateCreditCardInfo.put("reqBillToAddressLine2",reqBillToAddressLine2);  
			 updateCreditCardInfo.put("reqBillToAddressLine1",u_reqBillToAddressLine1);  
			 updateCreditCardInfo.put("reqCardType",reqCardType);  
			 updateCreditCardInfo.put("authAmount",authAmount);  
			 updateCreditCardInfo.put("reqBillToAddressCity",u_reqBillToAddressCity);  
			 updateCreditCardInfo.put("signedDateTime",signedDateTime);  
			 updateCreditCardInfo.put("reqCurrency",reqCurrency);  
			 updateCreditCardInfo.put("reqMerchantDefinedData25",reqMerchantDefinedData25);  
			 updateCreditCardInfo.put("reqReferenceNumber",reqReferenceNumber);  
			 updateCreditCardInfo.put("reqMerchantDefinedData22",reqMerchantDefinedData22);  
			 updateCreditCardInfo.put("reqMerchantDefinedData23",reqMerchantDefinedData23);  
			 updateCreditCardInfo.put("reqMerchantDefinedData21",reqMerchantDefinedData21);  
			 updateCreditCardInfo.put("authAvsCodeRaw",authAvsCodeRaw);  
			 updateCreditCardInfo.put("transactionId",transactionId);  
			 updateCreditCardInfo.put("authTime",authTime);  
			 updateCreditCardInfo.put("message",message);  
			 updateCreditCardInfo.put("authResponse",authResponse);  
			 updateCreditCardInfo.put("reqConsumerId",userSPSID);  
			 updateCreditCardInfo.put("reqProfileId",reqProfileId);  
			 updateCreditCardInfo.put("reqTransactionUuId",reqTransactionUuId);  
			 updateCreditCardInfo.put("reqPaymentToken",reqPaymentToken);  
			 updateCreditCardInfo.put("authTransRefNo",authTransRefNo);  
			 updateCreditCardInfo.put("reqBillToSurname",lastName);  
			 updateCreditCardInfo.put("reqBillToForename",firstName);  
			 updateCreditCardInfo.put("reqBillToEmail",email);  
		     updateCreditCardInfo.put("accessKey",accessKey);
			 return updateCreditCardInfo;
		}	 

		
				
		private ResponseSpecification createTeacherResponseValidator()

		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("userName",equalTo(email))
			//.expectBody("modifiedDate",equalTo(""))
			//.expectBody("registrationDate",equalTo(new Date()))
			.expectBody("schoolId",equalTo(schoolId))
			.expectBody("orgZip",is(not(empty())))
			.expectBody("userType",equalTo(userType))
			.expectBody("isEducator",is(not(empty())))
			.expectBody("cac",is(not(empty())))
			.expectBody("cacId",is(not(empty())))
			.expectBody("isIdUsed",is(not(empty())))
			.expectBody("isEnabled",is(not(empty())))
			.expectBody("schoolUcn",is(not(empty())))
			.expectBody("email",equalTo(email))
			.expectBody("firstName",equalTo(firstName))
			.expectBody("lastName",equalTo(lastName))
			//.expectBody("password",equalTo(password))
			.expectBody("spsId",is(not(empty())));
			return rspec.build();		
		}
		
		private ResponseSpecification getCreditCardResponseValidator()

		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("count",is(not(empty())))
			.rootPath("wallet")
			.expectBody("id[0]",is(not(empty())))
			.expectBody("ccmonth[0]",equalTo("03"))
			.expectBody("cbspt[0]",equalTo("01019648212121"))
			.expectBody("cnm[0]",is(not(empty())))
			.expectBody("phoneExtNumber[0]",is(not(empty())))
			.expectBody("email[0]",is(not(empty())))
			.expectBody("zip[0]",equalTo("11372"))
			.expectBody("firstName[0]",equalTo("Kerry"))
			.expectBody("address1[0]",equalTo("34-21 70th Street"))
			.expectBody("address2[0]",equalTo("line2"))
			.expectBody("ccBrand[0]",equalTo("V"))
			.expectBody("phoneNumber[0]",equalTo("7189916545"))
			.expectBody("lastName[0]",equalTo("Moore"))
			.expectBody("ccnumber[0]",equalTo("xxxxxxxxxxxx1111"))
			.expectBody("city[0]",equalTo("Jackson Heights"))
			.expectBody("success[0]",equalTo("test"))
			.expectBody("default[0]",equalTo("true"))
			.expectBody("state[0]",equalTo("NY"));
			return rspec.build();
		}
		
		private ResponseSpecification getUpdatedCreditCardResponseValidator()

		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("count",is(not(empty())))
			.rootPath("wallet")
			.expectBody("id[0]",is(not(empty())))
			.expectBody("ccmonth[0]",equalTo("04"))
			.expectBody("cbspt[0]",equalTo("01019648212121"))
			.expectBody("cnm[0]",is(not(empty())))
			.expectBody("phoneExtNumber[0]",is(not(empty())))
			.expectBody("email[0]",is(not(empty())))
			.expectBody("zip[0]",equalTo("11427"))
			.expectBody("firstName[0]",equalTo("Kerry"))
			.expectBody("address1[0]",equalTo("89-24 215th Street"))
			.expectBody("address2[0]",equalTo("line2"))
			.expectBody("ccBrand[0]",equalTo("V"))
			.expectBody("phoneNumber[0]",equalTo("7189916545"))
			.expectBody("lastName[0]",equalTo("Moore"))
			.expectBody("ccnumber[0]",equalTo("xxxxxxxxxxxx1111"))
			.expectBody("city[0]",equalTo("Queens Village"))
			.expectBody("success[0]",equalTo("test"))
			.expectBody("default[0]",equalTo("true"))
			.expectBody("state[0]",equalTo("NY"));
			return rspec.build();
		}
		
	}
