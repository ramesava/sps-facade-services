package com.scholastic.facade.Automation;




import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.ResponseSpecification;

	public class SPSSchoolEditServiceTest extends BaseTest
	{
		//Login for Session Id and TSP Id
		private String user;
		private String password ="passed1";
		private boolean singleToken = true;
		private boolean userForWS = false;
		private boolean addCookie = true;
		private boolean addExpiration = true;
		private String sps_session;
		private String sps_tsp;
		private String userSPSID;
		
		
		//private String title="MR";
		private String firstName="Eric";
		private String lastName="Jackson";
		private String email;
		private String schoolId="453926";
		private String i_schoolSPSID="30152192";
		private List<String> userType=new ArrayList<String>();
		private String teacherSPSID;
		
		//Home School
		private String h_name ="Home School Inc. 2015";
		private String h_address1 ="568 Broadway";
		private String h_address2 ="";
		private String h_address3 ="";
		private String h_address4 ="";
		private String h_address5 ="";
		private String h_city ="NYC";
		private String h_state ="NY";
		private String h_zipcode ="10012";
		private String h_country ="";
		private String h_poBox ="";
		private String h_county ="";
		private String h_phone ="2123438565";
		private String h_reportingSchoolType ="HOS";
		private String h_schoolSPSID;
		
		//Manual School
		private String m_name ="Manual School Inc. 2016";
		private String m_address1 ="550 Broadway";
		private String m_address2 ="";
		private String m_address3 ="";
		private String m_address4 ="";
		private String m_address5 ="";
		private String m_city ="NYC";
		private String m_state ="NY";
		private String m_zipcode ="10012";
		private String m_country ="";
		private String m_poBox ="";
		private String m_county ="";
		private String m_phone ="2123434535";
		private String m_reportingSchoolType ="PS";
		private String m_schoolSPSID;
		
		//Review School
		private String r_spsId="";
		private String r_name ="Sch Inc.";
		private String r_schoolUCN="";
		private String r_borderId="";
		private String r_address1 ="568 Broadway";
		private String r_address2 ="";
		private String r_address3 ="";
		private String r_address4 ="";
		private String r_address5 ="";
		private String r_city ="NYC";
		private String r_state ="NY";
		private String r_creditCardState ="";
		private String r_zipcode ="10012";
		private String r_country ="";
		private String r_poBox ="";
		private String r_county ="";
		private String r_phone ="2123436100";
		private String r_schoolNote ="";
		private String r_schoolStatus="";
		private String r_groupType="";
		private String r_internatIonalDomestIcCode="";
		private String r_publicPrivateKey="";
		private String r_uspsTypeCode="0";
		private String r_addressTypeCode="";
		private String r_lastModifiedDate="";
		private String r_modifiedDate="";
		private String r_createdSource="";
		private String r_createDate="";
		private String r_reportingSchoolType ="";
		private String r_survivingUcn="";
		
			
		//End Points
		private static final String ENDPOINT_TEACHER_REGISTRATION="/spsuser/?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_CHANGE_SCHOOL="/spsuser/{spsId}/spsorganization?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_CREATE_SCHOOL="/spsorganization/?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_REVIEW_SCHOOL="/spsorganization/{spsId}/reviewschooladdress?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_GET_SCHOOL="/spsuser/{spsId}?clientId=SPSFacadeAPI";
				
	
	 		
	@Test
		public void createTeacherLogInAndEditToPutanInternationalSchoolTest() throws InterruptedException
	{
		System.out.println("##################################################################################");	
		System.out.println("*************** Edit School-option 1 international Existing Schools***************");
		System.out.println("##################################################################################");
		//Create Teacher
		ExtractableResponse<Response> createTeacherResponse=
				given()
						.log().all()
						.contentType("application/json")
						.body(createTeacherPayload()).
				when()
						.post(ENDPOINT_TEACHER_REGISTRATION).
				then()
						.statusCode(201)
						.spec(createTeacherResponseValidator())
						.extract();		
		teacherSPSID=createTeacherResponse.path("spsId");
		user=createTeacherResponse.path("userName");
		System.out.println(">>>>>>>Teacher SPSID is: "+teacherSPSID);
		System.out.println(">>>>>>>User Name is: "+user);
		
		Thread.sleep(6000L);
		
		//Authentication
		ExtractableResponse<Response> createLogInResponse=
				given()
						.contentType("application/json")
		                .body(createLogInPayload()).
				when()
						.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
				then()
						.statusCode(200)
						.extract();	
		sps_session=createLogInResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInResponse.path("SPS_TSP.value");
		System.out.println(createLogInResponse.path("SPS_UD.value"));
		userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
		
		System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
		
		//Change School to Existing International School
		
		ExtractableResponse<Response> changedToInternationalSchoolResponse=
				given()
						.log().all()
						.pathParam("spsId",userSPSID)
						.cookies("SPS_SESSION",sps_session)
						.cookie("SPS_TSP",sps_tsp)
						.contentType("application/json")
						.body(changedToInternationalSchoolPayload()).
				when()
						.put(ENDPOINT_CHANGE_SCHOOL).
				then()
						.statusCode(200)
						.extract();
		teacherSPSID=changedToInternationalSchoolResponse.path("spsId");
		System.out.println(">>>>>>>Teacher SPSID is: "+teacherSPSID);
		// Get School Info after change school
		 ExtractableResponse<Response> afterChangedSchoolGetInfoResponse=
		 				given()
		 						.log().all()
		 						.pathParam("spsId",teacherSPSID)
		 						.contentType("application/json").
		 				when()
		 						.get(ENDPOINT_GET_SCHOOL).
		 				then()
		 						.statusCode(200)
		 						.spec(changedToInternationalSchoolResponseValidator())
								.extract();		
		System.out.println("******************** Teacher's Initial Registerd School Was **********************");
		System.out.println(createTeacherResponse.asString());
		System.out.println("**********************************************************************************");
		
		System.out.println("******************** Changed To International School is **************************");
		System.out.println(afterChangedSchoolGetInfoResponse.asString());
		System.out.println("**********************************************************************************");
		
	}	
	
	@Test
		public void createHomeSchoolAndChangeUserSchoolToHomeSchoolTest() throws InterruptedException
	{
		System.out.println("####################################################################################");	
		System.out.println("*************** Service to edit school - (option 1) for homeschooler.***************");
		System.out.println("####################################################################################");
		
		ExtractableResponse<Response> createTeacherResponse=
				given()
						.log().all()
						.contentType("application/json")
						.body(createTeacherPayload()).
				when()
						.post(ENDPOINT_TEACHER_REGISTRATION).
				then()
						.statusCode(201)
						.spec(createTeacherResponseValidator())
						.extract();		
		teacherSPSID=createTeacherResponse.path("spsId");
		user=createTeacherResponse.path("userName");
		System.out.println(">>>>>>User SPSID is: "+teacherSPSID);
		System.out.println(">>>>>>>User Name is: "+user);
		
		//Create Home School
		ExtractableResponse<Response> createHomeSchoolResponse=
				given()
						.log().all()
						.contentType("application/json")
						.body(createHomeSchoolPayload()).
				when()
						.post(ENDPOINT_CREATE_SCHOOL).
				then()
						.statusCode(201)
						.spec(createHomeSchoolResponseValidator())
						 .extract();	
		h_schoolSPSID=createHomeSchoolResponse.path("spsId");
		//System.out.println(createHomeSchoolResponse.asString());
		System.out.println(">>>>>>Home School SPSID is: "+h_schoolSPSID);
		
		Thread.sleep(6000L);
		//Authentication
		ExtractableResponse<Response> createLogInResponse=
				given()
						.contentType("application/json")
		                .body(createLogInPayload()).
				when()
						.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
				then()
						.statusCode(200)
						.extract();	
		
		sps_session=createLogInResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInResponse.path("SPS_TSP.value");
		System.out.println(createLogInResponse.path("SPS_UD.value"));
		userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
		
		System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);	
		
		//Change School to Home School
		
		ExtractableResponse<Response> changedToHomeSchoolResponse=
				given()
						.log().all()
						.pathParam("spsId",userSPSID)
						.cookies("SPS_SESSION",sps_session)
						.cookie("SPS_TSP",sps_tsp)
						.contentType("application/json")
						.body(changedToHomeSchoolPayload()).
				when()
						.put(ENDPOINT_CHANGE_SCHOOL).
				then()
						.statusCode(200)
						.extract();
		userSPSID=changedToHomeSchoolResponse.path("spsId");	
		System.out.println("User SPSID is: "+userSPSID);
		// Get School Info after change school
		ExtractableResponse<Response> afterChangedSchoolGetInfoResponse=
				 given()
				 		.log().all()
				 		.pathParam("spsId",teacherSPSID)
				 		.contentType("application/json").
				 when()
				 		.get(ENDPOINT_GET_SCHOOL).
				 then()
				 		.statusCode(200)
				 		.spec(changedToHomeSchoolResponseValidator())
						.extract();		
		System.out.println("******************** Teacher's Initial Registerd School Was *********************");
		System.out.println(createTeacherResponse.asString());
		System.out.println("**********************************************************************************");
				
		System.out.println("******************** Changed To Home School is ***********************************");
		System.out.println(afterChangedSchoolGetInfoResponse.asString());
		System.out.println("**********************************************************************************");
	}
	
	@Test
		public void createManualSchoolAndChangeUserSchoolToManualSchoolTest() throws InterruptedException
	{
		System.out.println("####################################################################################");	
		System.out.println("*************** Service to edit school - (option 1) for Manual School.***************");
		System.out.println("####################################################################################");
		
		ExtractableResponse<Response> createTeacherResponse=
				given()
						.log().all()
						.contentType("application/json")
						.body(createTeacherPayload()).
				when()
						.post(ENDPOINT_TEACHER_REGISTRATION).
				then()
						.statusCode(201)
						.spec(createTeacherResponseValidator())
						.extract();		
		teacherSPSID=createTeacherResponse.path("spsId");
		user=createTeacherResponse.path("userName");
		System.out.println(">>>>>>User SPSID is: "+teacherSPSID);
		System.out.println(">>>>>>>User Name is: "+user);
		
		//Create Manual School
		ExtractableResponse<Response> createManualSchoolResponse=
				given()
						.log().all()
						.contentType("application/json")
						.body(createManualSchoolPayload()).
				when()
						.post(ENDPOINT_CREATE_SCHOOL).
				then()
						.statusCode(201)
						.spec(createManualSchoolResponseValidator())
						 .extract();	
		m_schoolSPSID=createManualSchoolResponse.path("spsId");
		//System.out.println(createManualSchoolResponse.asString());
		System.out.println(">>>>>>Manual School SPSID is: "+m_schoolSPSID);
		
		Thread.sleep(6000L);
		//Authentication
		ExtractableResponse<Response> createLogInResponse=
				given()
						.contentType("application/json")
		                .body(createLogInPayload()).
				when()
						.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
				then()
						.statusCode(200)
						.extract();	
		sps_session=createLogInResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInResponse.path("SPS_TSP.value");
		System.out.println(createLogInResponse.path("SPS_UD.value"));
		userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
		
		System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);	
		
		//Change School to Manual School
		
		ExtractableResponse<Response> changedToManualSchoolResponse=
				given()
						.log().all()
						.pathParam("spsId",userSPSID)
						.cookies("SPS_SESSION",sps_session)
						.cookie("SPS_TSP",sps_tsp)
						.contentType("application/json")
						.body(changedToManualSchoolPayload()).
				when()
						.put(ENDPOINT_CHANGE_SCHOOL).
				then()
						.statusCode(200)
						//.spec(changedToManualSchoolResponseValidator())
						.extract();
		userSPSID=changedToManualSchoolResponse.path("spsId");	
		System.out.println("User SPSID is: "+userSPSID);
		// Get School Info after change school
		ExtractableResponse<Response> afterChangedSchoolGetInfoResponse=
				 given()
				 		.log().all()
				 		.pathParam("spsId",teacherSPSID)
				 		.contentType("application/json").
				 when()
				 		.get(ENDPOINT_GET_SCHOOL).
				 then()
				 		.statusCode(200)
				 		.spec(changedToManualSchoolResponseValidator())
						.extract();		
		System.out.println("******************** Teacher's Initial Registerd School Was *********************");
		System.out.println(createTeacherResponse.asString());
		System.out.println("**********************************************************************************");
				
		System.out.println("******************** Changed To Manual School is ***********************************");
		System.out.println(afterChangedSchoolGetInfoResponse.asString());
		System.out.println("**********************************************************************************");
	}
		
	@Test
		public void createSchoolAndSendInformationForReviewTest() throws InterruptedException
	{
		System.out.println("###################################################################################################");	
		System.out.println("*** Service to edit School:(option 2&3)The school name is correct, but the address is incorrect.***");
		System.out.println("###################################################################################################");
		
		ExtractableResponse<Response> createTeacherResponse=
				given()
						.log().all()
						.contentType("application/json")
						.body(createTeacherPayload()).
				when()
						.post(ENDPOINT_TEACHER_REGISTRATION).
				then()
						.statusCode(201)
						.spec(createTeacherResponseValidator())
						.extract();		
		teacherSPSID=createTeacherResponse.path("spsId");
		user=createTeacherResponse.path("userName");
		System.out.println(">>>>>>User SPSID is: "+teacherSPSID);
		System.out.println(">>>>>>>User Name is: "+user);
		
		Thread.sleep(7000L);
		//Authentication
		ExtractableResponse<Response> createLogInResponse=
				given()
						.contentType("application/json")
		                .body(createLogInPayload()).
				when()
						.post("http://fs-iam-spsapifacade-qa1.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
				then()
						.statusCode(200)
						.extract();	
		sps_session=createLogInResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInResponse.path("SPS_TSP.value");
		System.out.println(createLogInResponse.path("SPS_UD.value"));
		userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
		
		System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);	
		
		//Send School Info for Review
		
		ExtractableResponse<Response> sendSchoolInfoForReviewResponse=
				given()
						.log().all()
						.pathParam("spsId",userSPSID)
						.cookies("SPS_SESSION",sps_session)
						.cookie("SPS_TSP",sps_tsp)
						.contentType("application/json")
						.body(sendSchoolInfoForReviewPayload()).
				when()
						.post(ENDPOINT_REVIEW_SCHOOL).
				then()
						.statusCode(200)
						//.spec(sendSchoolInfoForReviewResponseValidator())
						.extract();
		//userSPSID=createCapturePaymentResponse.path("spsId");	
		//System.out.println("User SPSID is: "+userSPSID);
		System.out.println("******************** Teacher's Initial Registerd School Was *********************");
		System.out.println(createTeacherResponse.asString());
		System.out.println("**********************************************************************************");
				
		System.out.println("********** Response When Send School Info For Review ***************");
		System.out.println(sendSchoolInfoForReviewResponse.asString());
		System.out.println("*******************************************************************");
	}
	

		private String getEmail()
	{
		email="sps_qa"+String.valueOf(System.currentTimeMillis())+"@sample.com";
		return email;
	
	}
		
		private Map<String, Object> createLogInPayload()

		{
			Map<String, Object> createLogInInfo = new HashMap<String, Object>();
			createLogInInfo.put("user",user);
			createLogInInfo.put("password",password);
			createLogInInfo.put("singleToken",singleToken);
			createLogInInfo.put("userForWS", userForWS);
			createLogInInfo.put("addCookie",addCookie);
			createLogInInfo.put("addExpiration",addExpiration);
			//System.out.println("!!!!!!!!!!!!!"+createLogInInfo.toString());
			return createLogInInfo;
		
		}
		
		private Map<String, Object> createTeacherPayload()
		
		{
			if(email==null)
			{
				email=getEmail();
			}
			Map<String, Object> teacherRegistrationInfo = new HashMap<String, Object>();
			teacherRegistrationInfo.put("firstName",firstName);
			teacherRegistrationInfo.put("lastName",lastName);
			teacherRegistrationInfo.put("email",getEmail());
			userType.add("TEACHER");
			teacherRegistrationInfo.put("userType", userType);
			teacherRegistrationInfo.put("password",password);
			teacherRegistrationInfo.put("userName",email);
			teacherRegistrationInfo.put("schoolId",schoolId);
			return teacherRegistrationInfo;
			
		}
		
		private Map<String, Object> changedToInternationalSchoolPayload()
		
		{
			Map<String, Object> existingInternationalSchoolInfo = new HashMap<String, Object>();
			existingInternationalSchoolInfo.put("spsId",i_schoolSPSID);
			return existingInternationalSchoolInfo;
			
		}
		
		private Map<String, Object> createHomeSchoolPayload()
		
		{
			Map<String, Object> createHomeSchoolInfo = new HashMap<String, Object>();
			createHomeSchoolInfo.put("name",h_name );
			createHomeSchoolInfo.put("address1",h_address1);
			createHomeSchoolInfo.put("address2",h_address2);
			createHomeSchoolInfo.put("address3", h_address3);
			createHomeSchoolInfo.put("address4",h_address4);
			createHomeSchoolInfo.put("address5",h_address5);
			createHomeSchoolInfo.put("city",h_city);
			createHomeSchoolInfo.put("state", h_state);
			createHomeSchoolInfo.put("zipcode",h_zipcode);
			createHomeSchoolInfo.put("country",h_country);
			createHomeSchoolInfo.put("poBox",h_poBox);
			createHomeSchoolInfo.put("county",h_county);
			createHomeSchoolInfo.put("phone", h_phone);
			createHomeSchoolInfo.put("reportingSchoolType",h_reportingSchoolType);
			return createHomeSchoolInfo;
			
		}
		
		private Map<String, Object> changedToHomeSchoolPayload()
		
		{
			Map<String, Object> homeSchoolInfo = new HashMap<String, Object>();
			homeSchoolInfo.put("spsId",h_schoolSPSID);
			return homeSchoolInfo;
			
		}
		
		private Map<String, Object> createManualSchoolPayload()
		
		{
			Map<String, Object> createManualSchoolInfo = new HashMap<String, Object>();
			createManualSchoolInfo.put("name",m_name );
			createManualSchoolInfo.put("address1",m_address1);
			createManualSchoolInfo.put("address2",m_address2);
			createManualSchoolInfo.put("address3", m_address3);
			createManualSchoolInfo.put("address4",m_address4);
			createManualSchoolInfo.put("address5",m_address5);
			createManualSchoolInfo.put("city",m_city);
			createManualSchoolInfo.put("state", m_state);
			createManualSchoolInfo.put("zipcode",m_zipcode);
			createManualSchoolInfo.put("country",m_country);
			createManualSchoolInfo.put("poBox",m_poBox);
			createManualSchoolInfo.put("county",m_county);
			createManualSchoolInfo.put("phone", m_phone);
			createManualSchoolInfo.put("reportingSchoolType",m_reportingSchoolType);
			return createManualSchoolInfo;
			
		}
		
		private Map<String, Object> changedToManualSchoolPayload()
		
		{
			Map<String, Object> manualSchoolInfo = new HashMap<String, Object>();
			manualSchoolInfo.put("spsId",m_schoolSPSID);
			return manualSchoolInfo;
			
		}
		
private Map<String, Object> sendSchoolInfoForReviewPayload()
		
		{
			Map<String, Object> reviewSchoolInfo = new HashMap<String, Object>();
			reviewSchoolInfo.put("spsId",r_spsId);
			reviewSchoolInfo.put("name",r_name );
			reviewSchoolInfo.put("schoolUCN",r_schoolUCN );
			reviewSchoolInfo.put("borderId",r_borderId );
			reviewSchoolInfo.put("address1",r_address1);
			reviewSchoolInfo.put("address2",r_address2);
			reviewSchoolInfo.put("address3", r_address3);
			reviewSchoolInfo.put("address4",r_address4);
			reviewSchoolInfo.put("address5",r_address5);
			reviewSchoolInfo.put("city",r_city);
			reviewSchoolInfo.put("state", r_state);
			reviewSchoolInfo.put("creditCardState", r_creditCardState);
			reviewSchoolInfo.put("zipcode",r_zipcode);
			reviewSchoolInfo.put("country",r_country);
			reviewSchoolInfo.put("poBox",r_poBox);
			reviewSchoolInfo.put("county",r_county);
			reviewSchoolInfo.put("phone", r_phone);
			reviewSchoolInfo.put("schoolNote", r_schoolNote);
			reviewSchoolInfo.put("schoolStatus", r_schoolStatus);
			reviewSchoolInfo.put("groupType", r_groupType);
			reviewSchoolInfo.put("internatIonalDomestIcCode", r_internatIonalDomestIcCode);
			reviewSchoolInfo.put("publicPrivateKey", r_publicPrivateKey);
			reviewSchoolInfo.put("uspsTypeCode", r_uspsTypeCode);
			reviewSchoolInfo.put("addressTypeCode", r_addressTypeCode);
			reviewSchoolInfo.put("lastModifiedDate", r_lastModifiedDate);
			reviewSchoolInfo.put("modifiedDate", r_modifiedDate);
			reviewSchoolInfo.put("createdSource", r_createdSource);
			reviewSchoolInfo.put("createDate", r_createDate);
			reviewSchoolInfo.put("reportingSchoolType",r_reportingSchoolType);
			reviewSchoolInfo.put("survivingUcn", r_survivingUcn);
			return reviewSchoolInfo;
			
		}

		private ResponseSpecification createTeacherResponseValidator()

		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("userName",equalTo(email))
			//.expectBody("modifiedDate",equalTo(""))
			//.expectBody("registrationDate",equalTo(new Date()))
			.expectBody("schoolId",equalTo(schoolId))
			.expectBody("orgZip",is(not(empty())))
			.expectBody("userType",equalTo(userType))
			.expectBody("isEducator",is(not(empty())))
			.expectBody("cac",is(not(empty())))
			.expectBody("cacId",is(not(empty())))
			.expectBody("isIdUsed",is(not(empty())))
			.expectBody("isEnabled",is(not(empty())))
			.expectBody("schoolUcn",is(not(empty())))
			.expectBody("email",equalTo(email))
			.expectBody("firstName",equalTo(firstName))
			.expectBody("lastName",equalTo(lastName))
			//.expectBody("password",equalTo(password))
			.expectBody("spsId",is(not(empty())));
			return rspec.build();		
		}
	
		private ResponseSpecification changedToInternationalSchoolResponseValidator()

		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("spsId",equalTo(teacherSPSID))
			.expectBody("schoolId",equalTo(i_schoolSPSID));
			return rspec.build();		
		}
		
		private ResponseSpecification createHomeSchoolResponseValidator()
		
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("name",equalTo("Home School Inc. 2015"))
			.expectBody("address1",equalTo("568 Broadway"))
			.expectBody("address2",isEmptyString())
			.expectBody("address3",isEmptyString())
			.expectBody("address4",isEmptyString())
			.expectBody("address5",isEmptyString())
			.expectBody("city",equalTo("NYC"))
			.expectBody("state",equalTo("NY"))
			.expectBody("country",isEmptyString())
			.expectBody("poBox",isEmptyString())
			.expectBody("phone",equalTo("2123438565"))
			.expectBody("reportingSchoolType",equalTo("HOS"));
			return rspec.build();
		}
		
		private ResponseSpecification changedToHomeSchoolResponseValidator()

		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("spsId",equalTo(teacherSPSID))
			.expectBody("schoolId",equalTo(h_schoolSPSID));
			return rspec.build();		
		}
		
		private ResponseSpecification createManualSchoolResponseValidator()
		
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("name",equalTo("Manual School Inc. 2016"))
			.expectBody("address1",equalTo("550 Broadway"))
			.expectBody("address2",isEmptyString())
			.expectBody("address3",isEmptyString())
			.expectBody("address4",isEmptyString())
			.expectBody("address5",isEmptyString())
			.expectBody("city",equalTo("NYC"))
			.expectBody("state",equalTo("NY"))
			.expectBody("country",isEmptyString())
			.expectBody("poBox",isEmptyString())
			.expectBody("phone",equalTo("2123434535"))
			.expectBody("reportingSchoolType",equalTo("PS"));
			return rspec.build();
		}
		
		private ResponseSpecification changedToManualSchoolResponseValidator()

		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("spsId",equalTo(teacherSPSID))
			.expectBody("schoolId",equalTo(m_schoolSPSID));
			return rspec.build();
		}
		
	}
