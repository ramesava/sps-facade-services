package com.scholastic.facade.Automation;



import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;



import com.jayway.restassured.builder.ResponseSpecBuilder;


import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.ResponseSpecification;

	public class SPSUserTeacherPostServiceTest extends BaseTest
	{
		//private String title="MR";
		private String firstName="Robert";
		private String lastName="Kent";
		private String email;
		private String password="passed1";
		private String schoolId="453926";
		private List<String> userType=new ArrayList<String>();
		private String teacherSPSID;
		private String displayName="TicToc";
		private List<String> subjectInterest=new ArrayList<String>();
		private List<String> gradesRecommended=new ArrayList<String>();
		private List<String> interesParent=new ArrayList<String>();
		private String parentPrimaryRole="2";
		public String userName;
		
		//End Points
		private static final String ENDPOINT_TEACHER_REGISTRATION="/spsuser/?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_TEACHER_GET="/spsuser/{spsId}?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_TEACHER_UPDATE="/spsuser/{spsId}?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_TEACHER_DELETE="/spsuser/{spsId}?clientId=SPSFacadeAPI";
		//private static final String ENDPOINT_SOCIALMEDIA_REGISTRATION="/spssocial/registerprofile?clientId=SPSFacadeAPI";
		
		@Test
			public void createTeacherGetTest()
			{
			System.out.println("#########################################################");
			System.out.println("******* Light Teacher Registration and Get Teacher*******");
			System.out.println("#########################################################");
			// Light Teacher Registration
			ExtractableResponse<Response> createTeacherResponse=
					given()
							.log().all()
							.contentType("application/json")
							.body(createTeacherPayload()).
					when()
							.post(ENDPOINT_TEACHER_REGISTRATION).
					then()
							.statusCode(201)
							.spec(createTeacherResponseValidator())
							.extract();		


			teacherSPSID=createTeacherResponse.path("spsId");
			userName=createTeacherResponse.path("email");	
			System.out.println(teacherSPSID);
			
			// Get Teacher 
			ExtractableResponse<Response> getTeacherResponse=
					given()
							.pathParam("spsId",teacherSPSID).
					when()
							.get(ENDPOINT_TEACHER_GET).
					then()
							.statusCode(200)
							.extract();		
		System.out.println(getTeacherResponse.asString());
		}	

		@Test
		public void createTeacherUpdateGetTest()
		{
			System.out.println("#######################################################################");
			System.out.println("******* Light Teacher Registration plus Update and Get Teacher*********");
			System.out.println("#######################################################################");
			// Light Teacher Registration
			ExtractableResponse<Response> createTeacherResponse=
					given()
							.log().all()
							.contentType("application/json")
							.body(createTeacherPayload()).
					when()
							.post(ENDPOINT_TEACHER_REGISTRATION).
					then()
							.statusCode(201)
							.spec(createTeacherResponseValidator())
							.extract();		
				
			teacherSPSID=createTeacherResponse.path("spsId");
			System.out.println(teacherSPSID);
		
			// Update Teacher 
			
				given()
						.log().all()
						.pathParam("spsId",teacherSPSID)
						.contentType("application/json")
						.body(updateTeacherPayload()).
				when()
						.put(ENDPOINT_TEACHER_UPDATE).
				then()
						.statusCode(200)
						.extract();	
			
			teacherSPSID=createTeacherResponse.path("spsId");
			System.out.println(teacherSPSID);

			// Get Teacher
			ExtractableResponse<Response> getTeacherResponse=
					given()
							.pathParam("spsId",teacherSPSID).
					when()
							.get(ENDPOINT_TEACHER_GET).
					then()
							.statusCode(200)
							.spec(createTeacherUpdateResponseValidator())
							.extract();		
		System.out.println(getTeacherResponse.asString());
		}
	
	@Test
		public void createTeacherDeleteTest()
		{	
			System.out.println("############################################################");
			System.out.println("******* Light Teacher Registration and Delete Teacher*******");
			System.out.println("############################################################");
	
			// Light Teacher Registration
			ExtractableResponse<Response> createTeacherResponse=
					given()
							.log().all()
							.contentType("application/json")
							.body(createTeacherPayload()).
					when()
							.post(ENDPOINT_TEACHER_REGISTRATION).
					then()
							.statusCode(201)
							.spec(createTeacherResponseValidator())
							.extract();		

			teacherSPSID=createTeacherResponse.path("spsId");	
			System.out.println(teacherSPSID);
	
			// Delete Teacher 
			ExtractableResponse<Response> deleteTeacherResponse=
					given()
							.pathParam("spsId",teacherSPSID).
					when()
							.delete(ENDPOINT_TEACHER_DELETE).
					then()
							.statusCode(200)
							.extract();		
			System.out.println(deleteTeacherResponse.asString());
		}	
	
	
			
		private String getEmail()
		{
			email="sps_qa"+String.valueOf(System.currentTimeMillis())+"@sample.com";
			return email;
		
		}

		private Map<String, Object> createTeacherPayload()
		
		//Payload
		{
			Map<String, Object> teacherRegistrationInfo = new HashMap<String, Object>();
			teacherRegistrationInfo.put("firstName",firstName);
			teacherRegistrationInfo.put("lastName",lastName);
			teacherRegistrationInfo.put("email",getEmail());
			userType.add("TEACHER");
			teacherRegistrationInfo.put("userType", userType);
			teacherRegistrationInfo.put("password",password);
			teacherRegistrationInfo.put("userName",email);
			teacherRegistrationInfo.put("schoolId",schoolId);
			return teacherRegistrationInfo;
			
		}
		
		private Map<String, Object> updateTeacherPayload()
		
		// Update Payload
		{
			Map<String, Object> updateTeacherInfo = new HashMap<String, Object>();
			updateTeacherInfo.put("displayName",displayName);
			subjectInterest.add("5");
			subjectInterest.add("6");
			updateTeacherInfo.put("subjectInterest", subjectInterest);
			gradesRecommended.add("3");
			gradesRecommended.add("4");
			updateTeacherInfo.put("gradesRecommended", gradesRecommended);
			interesParent.add("1");
			interesParent.add("2");
			updateTeacherInfo.put("interesParent", interesParent);
			updateTeacherInfo.put("parentPrimaryRole",parentPrimaryRole);
			
			return updateTeacherInfo;
			
		}
		
		private ResponseSpecification createTeacherResponseValidator()
		
		//Light Teacher Registration Validation
		{
				ResponseSpecBuilder rspec=new ResponseSpecBuilder()
				.expectBody("userName",equalTo(email))
				//.expectBody("modifiedDate",equalTo(""))
				//.expectBody("registrationDate",equalTo(new Date()))
				.expectBody("schoolId",equalTo(schoolId))
				.expectBody("orgZip",is(not(empty())))
				.expectBody("userType",equalTo(userType))
				.expectBody("isEducator",is(not(empty())))
				.expectBody("cac",is(not(empty())))
				.expectBody("cacId",is(not(empty())))
				.expectBody("isIdUsed",is(not(empty())))
				.expectBody("isEnabled",is(not(empty())))
				.expectBody("schoolUcn",is(not(empty())))
				.expectBody("email",equalTo(email))
				.expectBody("firstName",equalTo(firstName))
				.expectBody("lastName",equalTo(lastName))
				//.expectBody("password",equalTo(password))
				.expectBody("spsId",is(not(empty())));
				return rspec.build();		
		}
		
		private ResponseSpecification createTeacherUpdateResponseValidator()
		
		//Update Teacher Validation
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
				.expectBody("userName",equalTo(email))
				//.expectBody("modifiedDate",equalTo(""))
				//.expectBody("registrationDate",equalTo(new Date()))
				.expectBody("displayName",equalTo(displayName))
				.expectBody("subjectInterest",equalTo(subjectInterest))
				.expectBody("gradesRecommended",equalTo(gradesRecommended))
				.expectBody("interesParent",equalTo(interesParent))
				.expectBody("parentPrimaryRole",equalTo(parentPrimaryRole))
				.expectBody("spsId",is(not(empty())));
				return rspec.build();		
		}
}
