package com.scholastic.facade.Automation;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;



import org.junit.Test;

import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;

public class  ExistingUserAuthenticationTest extends BaseTest
{
	SPSUserTeacherPostServiceTest registerUser=new SPSUserTeacherPostServiceTest();
	// User with password less than 7 characters.
	private String user ="qatestsample703@juno1.com";
	private String password ="pass";
	// Invalid Password
	private String password1 ="passwordx";
	//Valid Password
	private String user1 ="qatestsample704@juno1.com";
	private String password2 ="passed1";
	private boolean singleToken = true;
	private boolean userForWS = false;
	private boolean addCookie = true;
	private boolean addExpiration = true;
	// 10 times
	//private String user2 ="ppatel@juno1.com";
	private String user2;
	
	//End Points
	private static final String ENDPOINT_USER_LOGIN="spsuser/login?clientId=SPSFacadeAPI";
		
	@Test
	public void existingUserLoginWithLessThan7CharactersTest() 
	{	
		System.out.println("###############################################################################");
		System.out.println("********** Existing User Login With Password Less Than 7 Characters  **********");
		System.out.println("###############################################################################");
		ExtractableResponse<Response> existingUserLoginResponse=
					given() 
							.log().all()
							.contentType("application/json")
							.body(createLogInPayload()).
					when()
							.post(ENDPOINT_USER_LOGIN).
					then()
							.statusCode(200)
							.body("message",equalTo("Password must be minimum of 7 and maximum of 30 characters, should contain at least an alphabetic letter and a number.Spaces are not allowed."))
							//.body("message",equalTo("Password must be minimum of 7 characters, should contain at least an alphabetic letter and a number."))
							.body("success",equalTo("false"))
							//.body("userId",equalTo("89601279"))
							.extract();
		
		System.out.println(existingUserLoginResponse.asString());
						
		
	}
	
	@Test
	public void existingUserLoginWithInvalidpasswordTest() 
	{	
		System.out.println("###############################################################");
		System.out.println("********** Existing User Login With Invalid Password **********");
		System.out.println("###############################################################");
		ExtractableResponse<Response> existingUserLoginWithInvalidpasswordResponse=
					given()
							.contentType("application/json")
							.body(createInvalidPasswordLogInPayload()).
					when()
							.post(ENDPOINT_USER_LOGIN).
					then()
							.statusCode(200)
							.body("message",equalTo("Invalid credentials."))
							.body("success",equalTo("false"))
							.body("userId",equalTo(null))
							.extract();
		
		System.out.println(existingUserLoginWithInvalidpasswordResponse.asString());
		
	}
	@Test
	public void existingUserLoginWithValidPasswordTest() 
	{	
		System.out.println("###############################################################################");
		System.out.println("********** Existing User Login With Valid Password - PCI Compliance  **********");
		System.out.println("###############################################################################");
		ExtractableResponse<Response> existingUserLoginWithValidPasswordResponse=
					given()
							.contentType("application/json")
							.body(createLogInWithValidPasswordPayload()).
					when()
							.post(ENDPOINT_USER_LOGIN).
					then()
							.statusCode(200)
							//.body("message",equalTo("Password must be minimum of 7 characters, should contain at least an alphabetic letter and a number."))
							//.body("success",equalTo("false"))
							//.body("userId",equalTo("89601279"))
							.extract();
			
		System.out.println(existingUserLoginWithValidPasswordResponse.asString());
							
			
		}	
	
	
	


	@Test
	public void existingUserLoginWithInvalidPasswordTenTimesTest() throws InterruptedException
	{
		System.out.println("##########################################################################");
		System.out.println("********** Existing User Login With Invalid Password Ten Times  **********");
		System.out.println("##########################################################################");
		
		int count=0;
		registerUser.createTeacherGetTest();
		user2=registerUser.userName;
				
		for(int i=0;i<10;i++)
		{	
			Thread.sleep(10000L);
			ExtractableResponse<Response> existingUserLoginWithInvalidPasswordTenTimesResponse=
						given()
								.contentType("application/json")
								.body(createLogInTenTimesPayload()).
						when()
								.post(ENDPOINT_USER_LOGIN).
						then()
								.statusCode(200)
								.extract();
				count=count+1;
			if(i<=8)
			{
				System.out.println("Login Attempt :"+count);
				System.out.println(existingUserLoginWithInvalidPasswordTenTimesResponse.asString());
				 assertEquals(existingUserLoginWithInvalidPasswordTenTimesResponse.path("message"),"Invalid credentials.");
				 assertEquals(existingUserLoginWithInvalidPasswordTenTimesResponse.path("success"),"false");
				 assertEquals(existingUserLoginWithInvalidPasswordTenTimesResponse.path("userId"),null);
			}else
			{
				System.out.println("Log in Attempt :"+count);
				System.out.println(existingUserLoginWithInvalidPasswordTenTimesResponse.asString());
				 assertEquals(existingUserLoginWithInvalidPasswordTenTimesResponse.path("message"),"You have exceeded maximum number of invalid sign in attempts. For your security, your account has been locked and you must wait 30 minutes before attempting to sign in again. You may also reset your password to clear the lockout.");
				 assertEquals(existingUserLoginWithInvalidPasswordTenTimesResponse.path("success"),"false");
				 assertEquals(existingUserLoginWithInvalidPasswordTenTimesResponse.path("userId"),null);
			}	
		}
	}
		
	
	
	
	/*private String getEmail()
	{
		 String email="sps_qa"+String.valueOf(System.currentTimeMillis())+"@sample.com";
		 return email;
	
	}*/
	
	private Map<String, Object> createLogInPayload()
	{
		Map<String, Object> createLogInInfo = new HashMap<String, Object>();
		createLogInInfo.put("user",user );
		createLogInInfo.put("password",password);
		createLogInInfo.put("singleToken",singleToken);
		createLogInInfo.put("userForWS", userForWS);
		createLogInInfo.put("addCookie",addCookie);
		createLogInInfo.put("addExpiration",addExpiration);
		return createLogInInfo;
	
	}
	
	private Map<String, Object> createInvalidPasswordLogInPayload()
	{
		Map<String, Object> createLogInInfo = new HashMap<String, Object>();
		createLogInInfo.put("user",user );
		createLogInInfo.put("password",password1);
		createLogInInfo.put("singleToken",singleToken);
		createLogInInfo.put("userForWS", userForWS);
		createLogInInfo.put("addCookie",addCookie);
		createLogInInfo.put("addExpiration",addExpiration);
		return createLogInInfo;
	
	}
	
	private Map<String, Object> createLogInWithValidPasswordPayload()
	{
		Map<String, Object> createLogInInfo = new HashMap<String, Object>();
		createLogInInfo.put("user",user1 );
		createLogInInfo.put("password",password2);
		createLogInInfo.put("singleToken",singleToken);
		createLogInInfo.put("userForWS", userForWS);
		createLogInInfo.put("addCookie",addCookie);
		createLogInInfo.put("addExpiration",addExpiration);
		return createLogInInfo;
	
	}
	
	private Map<String, Object> createLogInTenTimesPayload()
	{
		Map<String, Object> createLogInInfo = new HashMap<String, Object>();
		createLogInInfo.put("user",user2 );
		createLogInInfo.put("password",password1);
		createLogInInfo.put("singleToken",singleToken);
		createLogInInfo.put("userForWS", userForWS);
		createLogInInfo.put("addCookie",addCookie);
		createLogInInfo.put("addExpiration",addExpiration);
		return createLogInInfo;
	
	}
	
	
}
